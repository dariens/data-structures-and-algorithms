#include<iostream>
#include <stdio.h>

using namespace std;

int main()
{
    int *p1;
    long int *p2;
    int a1=10;
    long int a2=20;

    p1=&a1;
    p2=&a2;

    cout<<"p1: "<<p1<<endl;
    cout<<"p2: "<<p2<<endl;
    cout<<"size of p1: "<<sizeof(p1)<<endl;
    cout<<"size of p2: "<<sizeof(p2)<<endl;
    cout<<"size of *p1: "<<sizeof(*p1)<<endl;
    cout<<"size of *p2: "<<sizeof(*p2)<<endl;

}
